class Anaimal {
    constructor(name) {
        this.name = name
        this.getNameInside = function() {
            console.log("animal name from inside function is: ", this.name)
        }
        this.getNameInside()
       }

    getNameOutside(){
        console.log("animal name from outside function is: ", this.name )
    }
}

let cat = new Anaimal('cat')
let dog = new Anaimal('dog')

console.log(cat.getNameOutside === dog.getNameOutside)
cat.getNameInside()
